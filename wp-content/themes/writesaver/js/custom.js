jQuery(document).ready(function () {
    (function ($) {
        // store the slider in a local variable
        var $window = $(window),
                flexslider1 = {vars: {}};
        // tiny helper function to add breakpoints
        function getGridSize1() {
            return (window.innerWidth < 568) ? 1 :
                    (window.innerWidth < 768) ? 2 :
                    (window.innerWidth < 992) ? 1 : 2;
        }
        $('.plan_flexslider').flexslider({
            animation: "slide",
            animationLoop: false,
            controlNav: false,
            directionNav: true,
            itemWidth: 300,
            itemMargin: 30,
            minItems: getGridSize1(), // use function to pull in initial value
            maxItems: getGridSize1(), // use function to pull in initial value
            move: 1,
            start: function (slider1) {
                flexslider1 = slider1;
            }
        });
        setTimeout(function () {
            $window.trigger('resize');
            $('.plan_flexslider').flexslider({
                animation: "slide",
                animationLoop: false,
                controlNav: false,
                directionNav: true,
                itemWidth: 230,
                itemMargin: 30,
                minItems: getGridSize1(), // use function to pull in initial value
                maxItems: getGridSize1(), // use function to pull in initial value
                move: 1,
                start: function (slider1) {
                    flexslider1 = slider1;
                }
            });
        }, 100);
        // check grid size on resize event
        $window.resize(function () {
            var gridSize1 = getGridSize1();

            flexslider1.vars.minItems = gridSize1;
            flexslider1.vars.maxItems = gridSize1;
        });
    }(jQuery));
    $('#login_regi_modal a .fa-remove').click(function (e) {
        e.stopPropagation();
        $('#login_regi_modal').fadeOut();
        $('#login_regi_modal').removeClass('open');
    });

    $('.open_regiPop').click(function (e) {
        e.stopPropagation();
        open_loginpop('register');
    });

    $("#login_pop #signin").validate({
        errorElement: 'span', //default input error message container
        errorClass: 'text-danger login_msg', // default input error message class         
        rules: {
            email: {
                required: true,
                email: true
            },
            pw: "required"
        },
        messages: {
            email: {
                required: "Email is required.",
                email: "Please enter a valid email."
            },
            pw: {
                required: "Password is required."
            }
        },
        submitHandler: function (form) {
            $('span.login_msg').remove();
            $('#loding').show();
            var alldata = $(' #signin').serialize();

            $.ajax({
                url: $('#ajax_url').val(),
                type: "POST",
                data: alldata + '&action=user_signin',
                //dataType: "html",
                success: function (data) {
                    if (data == 0)
                    {
                        $('#login_pop #signin_msg').html('<span  class="text-danger login_msg" >Your username and password do not match.</span>');
                        setTimeout(function () {
                            $(' .regi_pop .login_msg').fadeOut('slow');
                        }, 1000);
                    } else
                    {
                        $('#login_pop  #signin_msg').html('<span  class="text-success login_msg" >Login successful</span>');
                        if ($('#hid_mamu_level_id').val()) {
                            setTimeout(function () {
                                $(' .login_msg').fadeOut('slow');
                                $('#login_regi_modal').fadeOut();
                                $('#login_regi_modal').removeClass('open');
                                window.location.href = $('#plan_url').val() + '?plan=' + $('#hid_mamu_level_id').val();
                            }, 2000);
                        } else if ($('.hid_mamu_level_id').val()) {
                            setTimeout(function () {
                                $(' .login_msg').fadeOut('slow');
                                $('#login_regi_modal').fadeOut();
                                $('#login_regi_modal').removeClass('open');
                                window.location.href = $('.plan_url').val() + '?plan=' + $('.hid_mamu_level_id').val();
                            }, 2000);
                        } else {
                            setTimeout(function () {
                                $(' .login_msg').fadeOut('slow');
                                window.location.href = data;
                            }, 1000);
                        }
                    }
                    $('#loding').hide();
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    $('#loding').hide();
                    console.log(jqXHR + " :: " + textStatus + " :: " + errorThrown);
                }
            });
            return false;
        }
    });

    $("#signup").validate({
        errorElement: 'span', //default input error message container
        errorClass: 'text-danger reg_msg', // default input error message class  

        rules: {
            fname: "required",
            lname: "required",
            email: {
                required: true,
                email: true
            },

            pw: {
                required: true,
                minlength: 6,
                pwcheck: true,
            },
            cpw: {
                required: true,
                equalTo: "#pw",
                pwcheck: true,
                minlength: 6,
            }
        },
        messages: {
            fname: {
                required: "First name is required."
            },
            lname: {
                required: "Last name is required."
            },
            email: {
                required: "Email is required.",
                email: "Please enter a valid email address.",
            },
            pw: {
                required: "Password is required.",
                minlength: jQuery.validator.format("Please enter at least 6 characters."),
                pwcheck: "Password must contain an uppercase and lowercase letter, a number and a special character"
            },
            cpw: {
                required: "Confirm password is required.",
                equalTo: "Passwords do not match"
            }
        },
        submitHandler: function (form) {
            $('span.reg_msg').remove();
            if ($("#reg_remember").is(':checked') == false)
            {
                $("#spn_ChkAgree").show();
                return false;
            } else
            {
                $("#spn_ChkAgree").hide();
            }
            $('#loding').show();
            var alldata = $('#signup').serialize();
            $.ajax({
                url: $('#ajax_url').val(),
                type: "POST",
                data: alldata + '&action=user_signup',

                success: function (data) {
                    if (data == 1) {
                        $('#login_regi_modal span.user_mail').text($('#regi_pop input[name=email]').val());
                        $('#signup_msg').html('<span  class="text-success reg_msg" >Confirmation email has been send to your email account. Please verify your email.</span>');
                        $('.reg_msg').fadeOut('slow');
                        $('#register_section').hide();
                        $('#thankyou_section').show();
                    } else if (data == 2) {
                        $('#signup_msg').html('<span  class="text-danger reg_msg" >Email already exists. Please login to continue.</span>');
                        setTimeout(function () {
                            $('.reg_msg').fadeOut('slow');
                        }, 5000);
                    } else {
                        $('#signup_msg').html('<span  class="text-danger reg_msg" >An error occur while registering. Please try again.</span>');

                        setTimeout(function () {
                            $('.reg_msg').fadeOut('slow');
                        }, 5000);
                    }
                    $('#loding').hide();
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    $('#loding').hide();
                    console.log(jqXHR + " :: " + textStatus + " :: " + errorThrown);
                }
            });
        }
    });
    $.validator.addMethod("pwcheck", function (value) {
        return /^[A-Za-z0-9\d!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]*$/.test(value)
                && /[A-Z]/.test(value)
                && /[a-z]/.test(value)
                && /\d/.test(value)
                && /[!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]/.test(value);

    });
});

////$(window).trigger(resize);
//function resize() {
//    var editor_width = $(".hidden_scroll").width() + 16.5;
////    var editor_tot_width = $(".hidden_scroll").width() + 16.5;
//    $('.hidden_scroll').css('width', editor_width);
//    $('.parentscrollcontents').css('width', editor_width); 
//};

//$(window).resize(function (resize) {
//    setTimeout(function (){
//       var editor_width = $(".hidden_scroll").width();
//        var editor_tot_width = $(".hidden_scroll").width() + 16.5;
//        $('.hidden_scroll').css('width', editor_width);
//        $('.parentscrollcontents').css('width', editor_tot_width); 
//    },20);    
//});  


// Get Word count
function get_doc_word_count(val) {
    var wom = val.match(/\S+/g);
    return wom ? wom.length : 0;
}

function open_loginpop(login_mode) {
    if (login_mode == 'register') {
		
		
        $('.nav-tabs a[href="#regi_pop"]').tab('show');
    } else {
        $('.nav-tabs a[href="#login_pop"]').tab('show');
    }
	
	//console.log($('.nav-tabs a[href="#login_pop"]'));
	
    $('span.login_msg').remove();
    $('span.reg_msg').remove();
    $("#spn_ChkAgree").hide();
    $('#register_section').show();
    $('#thankyou_section').hide();
    if ($('#signup').length) {
        $('#signup')[0].reset();
    }
    if ($('#login_pop #signin').length) {
        $('#login_pop #signin')[0].reset();
    }

    $('#login_regi_modal').fadeIn();
    $('#login_regi_modal').addClass('open');
    $('#login_regi_modal').css('display', 'block');
    return false;
}

$('document').ready(function () {
    $(".changeable").focusout(function () {
        var element = $(this);
        if (!element.text().replace(" ", "").length) {
            element.empty();
        }
    });

    $('.close_tab .category_btn').on('click', function (evnt) {

        if ($('.close_tab #links').hasClass('open')) {
            $('.close_tab #links').removeClass('open').hide();
        } else {
            $('.close_tab #links').addClass('open').show();
        }
    });

    $('.only_alpha').keypress(function (evt) {
        /*
         var regex = new RegExp("^[a-zA-Z]+$");
         var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
         if (regex.test(str)) {
         return true;
         } else {
         e.preventDefault();
         return false;
         }
         */
        var charCode;
        if (window.event)
            charCode = window.event.keyCode;  //for IE
        else
            charCode = evt.which;  //for firefox
        if (charCode == 32) //for &lt;space&gt; symbol
            return true;
        if (charCode > 31 && charCode < 65) //for characters before 'A' in ASCII Table
            return false;
        if (charCode > 90 && charCode < 97) //for characters between 'Z' and 'a' in ASCII Table
            return false;
        if (charCode > 122) //for characters beyond 'z' in ASCII Table
            return false;
        return true;
    });

    $(".only_num").keypress(function (e) {
        if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
            return false;
        }
    });

//    setTimeout(function () {
//        $('#subscrib_pop').fadeIn('slow');
//        $('body').removeClass('pop_open');
//    }, 4000);
//    $('.pop_head a').click(function () {
//        $('#subscrib_pop').fadeOut('slow');
//        $('body').removeClass('pop_open');
//    });
    $('.create_new_doc button').click(function () {
        $('#pop_start').fadeIn('slow');
    });
    $('.pop_head a').click(function () {
        $('#pop_start').fadeOut('slow');
        $('#pop_start_Confirm').fadeOut('slow');

    });

//    $('.delete_track a').click(function () {
//        $('#pop_start_deleted_words').fadeIn('slow');
//    });

    $('#pop_start_deleted_words .pop_head a').click(function () {
        $('#pop_start_deleted_words').fadeOut('slow');
    });

    $('.pop_head a').click(function () {
        $('#pop_start').fadeOut('slow');
    });
    $('.btn_blue a').click(function () {
        //$('#pop_start').fadeIn('slow');		
    });
	
	$('.pmpro_btn-select').click(function () {
		
		var d=$(this);
sessionStorage['id_sku'] = d.data('id_sku');
sessionStorage['name'] = d.data('name');
sessionStorage['sku'] = d.data('sku');
sessionStorage['price'] = d.data('price');
sessionStorage['category'] = d.data('category');
sessionStorage['list'] = d.data('list');

		/*
ga('send', {
  hitType: 'event',
  eventCategory: 'Purchase now',
  eventAction: 'click',
  eventLabel: d.data('name')
});
		*/
		
ga('ec:addProduct', {
    'id': d.data('id_sku'),
    'name': d.data('name'),
    'category': d.data('category'),
    'position': d.data('id_sku')
  });
  ga('ec:setAction', 'click', {list: d.data('list')});

  // Send click with an event, then send user to product page.
  ga('send', 'event', 'UX', 'click', 'Results');
		
				
    });
	

	
    //Edit password popup
    $('.edit_link a.chg_pass').click(function () {
        $('#pass_modal').fadeIn('slow');
    });
    $('#pass_modal .pop_head a').click(function () {
        $('#pass_modal').fadeOut('slow');
    });

    $('#section1 a.instant_video_btn').on('click', function (e) {
        e.preventDefault();
        $(this).hide();
        $('#section1 .wpb_video_widget.instant_video iframe')[0].src += "&autoplay=1";

        return false;
    });

    $('#about_writesaver a.instant_video_btn').on('click', function (ev) {
        $(this).hide();
        $('#about_writesaver .wpb_video_widget.about_writesaver_video  iframe')[0].src += "&autoplay=1";
        ev.preventDefault();
    });

    /**
     * Enve, DM
     * Prevent notifications DOM populating.
     * We don't need this functions at all because we are using the CSS hover state.
     * TODO: ref. #7227 HTML Showing in Track Changes
     */

    /*$('.track_result .diff').mouseover(function () {

        var top_pos = $(this).position().top;
        var left_pos = $(this).position().left;
        var div_height = $('#txt_area_upload_doc').height();
        var div_width = $('#txt_area_upload_doc').width();
        var hover_text = '';
        var hover_top = '';

        if ($(this).hasClass("ins")) {
            if ($(this).next('.del').length) {
                hover_text = '<del class="diff old_word">' + $(this).next('.del').text() + '</del> - <ins class="diff old_word">' + $(this).text() + '</ins>';
            } else {
                hover_text = '<ins class="diff new_word">New Word</ins>';
            }
        } else {
            hover_text = '<del class="diff delete_word">Delete Word</del>';
        }
        $('.track_result').prepend(' <div class="hover_eff">' + hover_text + '</div>');

        if ($('#txt_area_upload_doc').scrollTop() > 0) {
            if (div_height < top_pos + 60)
                hover_top = top_pos - 60 + $('#txt_area_upload_doc').scrollTop();
            else
                hover_top = top_pos + 30 + $('#txt_area_upload_doc').scrollTop();
        } else
            hover_top = top_pos + 30;

        if ($('.hover_eff').width() + left_pos + 20 > div_width)
            left_pos = left_pos - hover_text.length;

        $('.hover_eff').css({
            "display": "block",
            "left": left_pos,
            "top": hover_top
        });
    });
    $('.track_result .diff').mouseleave(function () {
        $('.hover_eff').remove();
    });*/


    $('.not_editable').on({
        mouseenter: function () {

            $(".popup_text").remove();
            var proof_name = $(this).attr('data-proof_name');

            if (proof_name.trim() != '') {
                $('.hidden_scroll.check').append('<span class="popup_text">Proofread by ' + proof_name + '</span>');
                var p = $(this);
                var offset = p.offset();
                var offset_top = offset.top;
                if (offset_top < 0) {
                    $('.popup_text').css('top', 90);
                } else {
                    $('.popup_text').css('top', offset.top / 3);
                }
            }
        },
        mouseleave: function () {
            $(".popup_text").remove();
        }
    });
});
if ($(window).width() >= 768) {
    var window_height = $(window).height();
    $('.top_section').css('height', window_height);
    $('.instant').css('height', window_height);
    $('.highlight').css('height', window_height);
    $('.detection').css('height', window_height);
//$('.home_proofreader').css('height', window_height);

//    $(window).bind("resize", function () {
//        var window_height = $(window).height();
//        $('.top_section').css('height', window_height);
//        $('.instant').css('height', window_height);
//        $('.highlight').css('height', window_height);
//        $('.detection').css('height', window_height);
////        $('.home_proofreader').css('height', window_height);
//    });
}

$(function () {
    $("#typed").typed({
        // strings: ["Typed.js is a <strong>jQuery</strong> plugin.", "It <em>types</em> out sentences.", "And then deletes them.", "Try it out!"],
        stringsElement: $('#typed-strings'),
        typeSpeed: 100,
        backDelay: 5000,
        loop: true,
        contentType: 'html', // or text
        // defaults to false for infinite loop
        loopCount: false,
        callback: function () {
            foo();
        },
        resetCallback: function () {
            newTyped();
        }
    });

    $(".reset").click(function () {
        $("#typed").typed('reset');
    });

});

function newTyped() { /* A new typed object */
}

function foo() {
    console.log("Callback");
}



//tinymce.init({
//    selector: '.text_editor textarea',
//    height: 340,
//    menubar: false,
//    plugins: [
//        'advlist autolink lists link image charmap print preview anchor',
//        'searchreplace visualblocks code fullscreen',
//        'insertdatetime media table contextmenu paste code'
//    ],
//    toolbar: 'undo redo | insert | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
//    content_css: '//www.tinymce.com/css/codepen.min.css'
//
//});





$(window).scroll(function () {
    if ($(this).scrollTop() > 1) {
        $('.header').addClass("fixed");
    } else {
        $('.header').removeClass("fixed");
    }
});

jQuery(window).load(function () {
    jQuery(window).resize();
//    jQuery('#wptime-plugin-preloader').delay(300).fadeOut("slow");
//
//    setTimeout(wptime_plugin_remove_preloader, 100);
//    function wptime_plugin_remove_preloader() {
//        jQuery('#wptime-plugin-preloader').remove();
//    }
});
/*
$(window).load(function () {
    $('#become').flexslider({
        animation: "fade",
        directionNav: false
    });
});
*/
$(function () {
    $("#accordion").accordion({
        autoHeight: false,
        collapsible: true,
        heightStyle: "content",
        active: 0,
        animate: 300
    });
});

$('#responsiveTabsDemo').responsiveTabs({
    startCollapsed: 'accordion'
});

//Sticky Js
/*
$(window).scroll(function () {
    var bread_hei = $('.breadcum').outerHeight();
    if ($(window).scrollTop() > bread_hei) {
        $('.basic .wrapper-sticky').addClass('sticky-active');
        $('.basic .blog_category_sticky').addClass('sticky');
    } else {
        $('.basic .wrapper-sticky').removeClass('sticky-active');
        $('.basic .blog_category_sticky').removeClass('sticky');
    }
    if ($('.header.fixed').length) {
        headr_height = $('.header.fixed').outerHeight();
        $('.about_sticky').hcSticky({
            top: headr_height
        });
        $('.blog_category_sticky').hcSticky({
            top: headr_height
        });
    }
});
if ($('.header.fixed').lengh) {
    $(window).bind("resize", function () {
        $('.about_sticky').hcSticky({
            top: headr_height
        });
        $('.blog_category_sticky').hcSticky({
            top: headr_height
        });
    });
}*/

$('.about_sticky ul li a[href*="#"]:not([href="#"])').click(function () {
    if (location.pathname.replace(/^\//, '') === this.pathname.replace(/^\//, '') && location.hostname === this.hostname) {
        var target = $(this.hash);
        var header_height = $('header').outerHeight();
        target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
        if (target.length) {
            $('html, body').animate({
                scrollTop: target.offset().top - 90
            }, 200);
            return;
        }
    }

});
$('.about_sticky ul li ').on('click', function () {
    $('.about_sticky ul li.active').removeClass('active');
    $(this).addClass('active');
});




$('.category_full_list #links  li').on('click', function () {

    $('.blog_category_main .blog_listt:eq(' + eval($(this).index()) + ')').fadeIn("show").siblings().fadeOut();
});
$('#links ').on('click', 'li', function () {
    $('#links  li.active').removeClass('active');
    $(this).addClass('active');
});


//$('.basic_info_inner .category_full_listing #all_link  li').on('click', function () {
//    $('.basic_info_inner .blog_category_main .blog_listt:eq(' + eval($(this).index()) + ')').fadeIn("show").siblings().fadeOut();
//});
//$('.basic_info_inner #all_link ').on('click', 'li', function () {
//    $('#all_link  li.active').removeClass('active');
//    $(this).addClass('active');
//});

$('.category_btn').on('click', function (evnt) {
    evnt.preventDefault();
    evnt.stopPropagation();
    if ($('.category_full_list #links').hasClass('open')) {
        $('.category_full_list #links').removeClass('open').hide();
    } else {
        $('.category_full_list #links').addClass('open').show();
    }

    if ($('.blog_category_full_list #links').hasClass('open')) {
        $('.blog_category_full_list #links').removeClass('open').hide();
    } else {
        $('.blog_category_full_list #links').addClass('open').show();
    }
});

if ($(window).width() < 768) {
    $(".category_full_list #links li").click(function () {
        $('.category_full_list #links').hide();
    });
}

$('.help_list #links  li').on('click', function () {
    $('.help_center_blockmain .help_center_block:eq(' + eval($(this).index()) + ')').fadeIn("show").siblings().fadeOut();
});

$(function () {
    $(".accordion").accordion({
        autoHeight: false,
        collapsible: true,
        heightStyle: "content",
        active: 0,
        animate: 300
    });

});
$('.help_list #links  li').bind('click', function (e) {
    $(".accordion").accordion({
        autoHeight: false,
        collapsible: true,
        heightStyle: "content",
        active: 0,
        animate: 300
    });
    e.preventDefault();

});

$(document).ready(function (e) {
    $('.openre').click(function (e) {
        e.preventDefault();
        e.stopPropagation();
        $(this).addClass('open');
        $('.collapsible_content').slideDown('slow');
    });
    $('.closer').click(function (evnt) {
        evnt.preventDefault();
        evnt.stopPropagation();
        if ($('.openre').hasClass('open')) {
            $('.openre').removeClass('open');
            $('.collapsible_content').slideUp('slow');
        }
    });
    $('.collapsible_content').click(function (et) {
        et.preventDefault();
        et.stopPropagation();
    });
    $(document).click(function (ent) {
        ent.stopPropagation();
        $('.openre').removeClass('open');
        $('.collapsible_content').slideUp('slow');
    });

});


	function myga(par){
		
		var par;
	ga('require', 'ec');

		/*ga('send', {
	  hitType: 'event',
	  eventCategory: 'Track click', 
	  eventAction: 'click',
	  eventLabel: par
	});
		*/
		function checkout(cart) { 

	ga('ec:addProduct', {
      'id': sessionStorage['id_sku'],
      'name': sessionStorage['name'],
      //'category': sessionStorage['category'],
      'price': sessionStorage['price'],
     // 'quantity': 1
    });
		}
		
			ga('ec:setAction','checkout', {
				'option': par      // Used to specify additional info about a checkout stage, e.g. payment method.
			});
	ga('send', 'pageview');  	

	}

//$(window).load(function () {
//        var inner_col_acco = document.getElementsByClassName('vc_col-sm-8');
//        var col_acco = $(inner_col_acco).html();
//        //alert(col_acco);  
//        
//    //  create container  
//    var element = document.createElement("div");
//    element.className = "container";
//    element.setAttribute("id", "container");
//    element.appendChild(document.createTextNode('The man who mistook his wife for a hat'));
//    document.getElementById('faq').prepend(element);
//    
//    //  create blank      
////    var col_acco1 = document.createElement("div");
////    col_acco1.className = "wpb_column vc_column_container vc_col-sm-2";
////    col_acco1.appendChild(document.createTextNode(''));
////    document.getElementById('container').appendChild(col_acco0);
//    
//    //  create accordion      
//    var col_acco1 = document.createElement("div");
//    col_acco1.className = "wpb_column vc_column_container vc_col-sm-8";
//    //col_acco1.appendChild(document.createTextNode(col_acco));
////    col_acco1.append(col_acco);
//    document.getElementById('container').appendChild(col_acco1);
//    
//    
//    
//    //  create blank      
////    var col_acco3 = document.createElement("div");
////    col_acco3.className = "wpb_column vc_column_container vc_col-sm-2";
////    col_acco3.appendChild(document.createTextNode(''));
////    document.getElementById('container').appendChild(col_acco3);
//});

//$("#example-vertical").steps({
//    headerTag: "h3",
//    bodyTag: "section",
//    transitionEffect: "slideLeft",
//    stepsOrientation: "vertical"
//});




//Custom accordion js Start
//$('document').ready(function (){
//    $('.vc_toggle').click(function (){
//        if($(this).parent().children('.vc_toggle').hasClass('vc_toggle_active')){
//            $(this).parent().children('.vc_toggle').removeClass('vc_toggle_active');
//            $(this).parent().children('.vc_toggle').children('.vc_toggle_content').slideUp();
//        }else if($(this).parent().children('.vc_toggle').siblings('.vc_toggle').hasClass('vc_toggle_active')){
//            $(this).parent().children('.vc_toggle').removeClass('vc_toggle_active');
//            $(this).parent().children('.vc_toggle').children('.vc_toggle_content').slideUp();
//            $(this).addClass('vc_toggle_active');
//            $(this).children('.vc_toggle_content').slideDown();
//        }else{
//            $(this).addClass('vc_toggle_active');
//            $(this).children('.vc_toggle_content').slideDown();
//        }
//    });
//});
