var connectUsrs = "";

var socket = io.connect('http://52.4.184.33:3000');

//$( "#submybtn" ).click( function() {
//	var nameVal = $( "#nameInput" ).val();
//	var msg = $( "#messageInput" ).val();
//	
//	socket.emit( 'message', { name: nameVal, message: msg } );
//	 $("#messageInput").val("");
//	// Ajax call for saving datas
////	$.ajax({
////		url: "./ajax/insertNewMessage.php",
////		type: "POST",
////		data: { name: nameVal, message: msg },
////		success: function(data) {
////			
////		}
////	});
////	
//	
//});
//
//socket.on( 'message', function( data ) {
//	var actualContent = $( "#messages" ).html();
//	var newMsgContent = '<li> <strong>' + data.name + '</strong> : ' + data.message + '</li>';
//	var content = newMsgContent + actualContent;	
//	$( "#messages" ).html( content );                  
//});


var URL = "http://52.4.184.33:3000";
console.log("Connecting to " + URL);
var socket = io.connect(URL);

// on connection to server, ask for user's name with an anonymous callback
socket.on('connect', function () {
    console.log("connect Function called");
    var lst_user = [];
    $("input:hidden[name='hdnarrayofUsers[]']").each(function () {
        lst_user.push($(this).val());
    });
    // call the server-side function 'adduser' and send one parameter (value of prompt)
    socket.emit('adduser', lst_user);
});

//cursor position get


// listener, whenever the server emits 'updatechat', this updates the chat body
socket.on('updatechat', function (connectedUserNames, data, start, sectionId,docdtlid) {
    connectUsrs = connectedUserNames;

    $customer = $("#loggedCustomerId").val();
    $ProofReader = $("#loggedProofReaderId").val();
    if (connectedUserNames[0] != null) {
        for (var i = 0; i < connectedUserNames[0].length; i++)
        {
            if (connectedUserNames[0][i].fk_cust_id === $customer || connectedUserNames[0][i].fk_proofreader_id.toString() === $ProofReader)
            {

                if ($("#loggedProofReaderId").length > 0) {
                    if (connectedUserNames[0][i].fk_proofreader_id == $ProofReader) {
                        //$('#txt_area_upload_doc').html(data);

                        $("#txt_area_upload_doc>p").each(function (index) {
                            var docid = $(this).attr("data-id");
                            if (connectedUserNames[0][i].fk_doc_details_id == docid) {

                                // $('[data-id="' + docid + '"]').html("<p data-id=" + connectedUserNames[0][i].fk_doc_details_id + ">" + connectedUserNames[0][i].document_desc + "</p>");
                                // placeCaretAtEnd(document.getElementById("txt_area_upload_doc"));
                            }
                            //$("#txt_area_upload_doc").append("<p contenteditable='false'> " + data + "</p>");
                        });

                    }
                }
                if ($("#loggedCustomerId").length > 0)
                {
                    if (connectedUserNames[0][i].fk_cust_id == $customer)
                    {
                        //$('#txt_area_upload_doc').html(data);

                        $("#txt_area_upload_doc>p").each(function (index) {
                            var docid = $(this).attr("data-id");
                            if (connectedUserNames[0][i].fk_doc_details_id == docid) {
                                //alert(start + " " + connectedUserNames[0][i].document_desc.toString().substr(start, 1));

                                //connectedUserNames[0][i].document_desc.substr(0, index) + str + connectedUserNames[0][i].document_desc.substr(index + 1);

                                if (sectionId == "1") {
                                    var str = "<b class='testcursorsample'>&nbsp;</b>";
                                    var doc_desc = connectedUserNames[0][i].document_desc.replaceAt(start, str);
                                }
                                else if (sectionId == "2") {
                                    var str1 = "<b class='testcursorsamplegreen'>&nbsp;</b>";
                                    var doc_desc = connectedUserNames[0][i].document_desc.replaceAt(start, str1);
                                }
                                else if (sectionId == "3") {
                                    var str2 = "<b class='testcursorsampleblue'>&nbsp;</b>";
                                    var doc_desc = connectedUserNames[0][i].document_desc.replaceAt(start, str2);
                                }
                                else if (sectionId == "4") {
                                    var str3 = "<b class='testcursorsamplegreenyellow'>&nbsp;</b>";
                                    var doc_desc = connectedUserNames[0][i].document_desc.replaceAt(start, str3);
                                }
                                else if (sectionId == "5") {
                                    var str4 = "<b class='testcursorsampleorange'>&nbsp;</b>";
                                    var doc_desc = connectedUserNames[0][i].document_desc.replaceAt(start, str4);
                                }
                                else {
                                    var str = "<b class='testcursorsample'>&nbsp;</b>";
                                    var doc_desc = connectedUserNames[0][i].document_desc.replaceAt(start, str);
                                }
                                // $('[data-id="' + docid + '"]').html("<p data-id=" + connectedUserNames[0][i].fk_doc_details_id + ">" + connectedUserNames[0][i].document_desc + "</p>");
                                //$('[data-id="' + docid + '"]').html("<p data-id=" + connectedUserNames[0][i].fk_doc_details_id + ">" + doc_desc + "</p>");
                                $("div#txt_area_upload_doc>p[data-id='" + docdtlid + "']").html("<p data-id=" + connectedUserNames[0][i].fk_doc_details_id + ">" + doc_desc + "</p>");

                                //placeCaretAtEnd(document.getElementById("txt_area_upload_doc"));
                            }
                            //$("#txt_area_upload_doc").append("<p contenteditable='false'> " + data + "</p>");
                        });
                    }
                }

            }
        }
    }
});



// listener, whenever the server emits 'updateusers', this updates the username list
socket.on('updateusers', function (data) {
    $('#users').empty();
    $.each(data, function (key, value) {
        $('#users').append('<div>' + key + '</div>');
    });
});



$(function () {
    // when the client clicks SEND
    $('#txt_area_upload_doc').keyup(function (e) {
        var userSelection;
        if (window.getSelection) {
            userSelection = window.getSelection();
        }
        var start = userSelection.anchorOffset;
        //alert("client" + start);
        setTimeout(function ()
        {
            var message = $('#txt_area_upload_doc').html();
            var docdtlid = $('#txt_area_upload_doc').attr("data-id");
            var sectionId = $('#txt_area_upload_doc').attr("data-section");
            // tell server to execute 'sendchat' and send along one parameter
            if (connectUsrs != "")
            {
                for (var i = 0; i < connectUsrs[0].length; i++)
                {
                    if (connectUsrs[0][i].fk_doc_details_id == docdtlid)
                    {
                        connectUsrs[0][i].document_desc = message;
                        break;
                    }
                }
            }
            socket.emit('adduser', connectUsrs[0]);
            socket.emit('sendchat', message, start, sectionId,docdtlid);


        }, 100);

    });
//    $('div[contenteditable=true]').keydown(function (e) {
//        // trap the return key being pressed
//        if (e.keyCode == 13) {
//            // insert 2 br tags (if only one br tag is inserted the cursor won't go to the second line)
//            document.execCommand('insertHTML', false, '<br>');
//            // prevent the default behaviour of return key pressed
//            return false;
//        }
//    });
    // when the client hits ENTER on their keyboard
    $('#txt_area_upload_doc').keypress(function (e) {


        //  placeCaretAtEnd(txt_area_upload_doc);
//        if (e.which == 13) {
//            $(this).blur();
//            $('#txt_area_upload_doc').focus().click();
//        }
    });
});
function placeCaretAtEnd(el) {
    el.focus();
    if (typeof window.getSelection != "undefined" && typeof document.createRange != "undefined") {
        var range = document.createRange();
        range.selectNodeContents(el);
        range.collapse(false);
        var sel = window.getSelection();
        sel.removeAllRanges();
        sel.addRange(range);
    } else if (typeof document.body.createTextRange != "undefined") {
        var textRange = document.body.createTextRange();
        textRange.moveToElementText(el);
        textRange.collapse(false);
        textRange.select();
    }
}
String.prototype.replaceAt = function (index, replacement) {
    return this.substr(0, index) + replacement + this.substr(index);
}