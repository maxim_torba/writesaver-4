<?php
/*
 * Template Name: Thank you
 */

$level = $_REQUEST['level'];

//if (!is_user_logged_in() || $level == '') {
if (!is_user_logged_in() ) {
    echo '<script>window.location.href="' . get_site_url() . '"</script>';
    exit;
}
get_header();
?>
<section class="login">
    <div <?php if ($level == '') {?> class="breadcum" <?php }?>>
        <div class="container">
            <?php if ($level == '') {?>
            <div class="page_title">
                <?php the_title('<h1>', '</h1>'); ?>
            </div>
            <?php }?>
            <div class="confirmation_thank_you registration_thank_you">
                <div class="row">
                    <div class="col-md-offset-3 col-sm-6">
                        <div class="thank-you">
                            <div class="thank_msg">
                                <div class="thank_img">
                                    <img src="<?php echo get_template_directory_uri() ?>/images/check_thanku.png" alt="images">
                                </div>
                                <h2>Thank You for your Subscription</h2>
                                <p>Your request has been received.
                                    You'll also receive a confirmation email for your subscription.
                                    Didn’t receive the email? Check your spam folder just in case.
                                    If you really can't find it, contact us at contact@writesaver.co</p> 

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php
// Transaction Data
$trans = array('id'=>'1234', 'affiliation'=>'Acme Clothing',
               'revenue'=>'11.99', 'shipping'=>'5', 'tax'=>'1.29');


// Function to return the JavaScript representation of a TransactionData object.
function getTransactionJs(&$trans) {
  return <<<HTML
ga('ecommerce:addTransaction', {
  'id': '{$trans['id']}',
  'affiliation': '{$trans['affiliation']}',
  'revenue': '{$trans['revenue']}',
  'shipping': '{$trans['shipping']}',
  'tax': '{$trans['tax']}'
});
HTML;
}

// Function to return the JavaScript representation of an ItemData object.
function getItemJs(&$transId, &$item) {
  return <<<HTML
ga('ecommerce:addItem', {
  'id': '$transId',
  'name': '{$item['name']}',
  'sku': '{$item['sku']}',
  'category': '{$item['name']}',
  'price': '{$item['price']}',
  'quantity': '1'
});
HTML;
}

?>



<script>
console.log(sessionStorage);

if (sessionStorage['id_sku']!==undefined) {
	

ga('require', 'ec');

ga('ec:addProduct', {
  'id': sessionStorage['id_sku'],
  'name': sessionStorage['name'],
 // 'category': sessionStorage['name'],
  'price': sessionStorage['price'],
 // 'quantity': '1'
});


ga('ec:setAction', 'purchase', {
  'id': sessionStorage['id_sku'],
  'name': sessionStorage['name'],
 // 'category': sessionStorage['name'],
  'revenue': sessionStorage['price'],
  
});

ga('send', 'pageview');  



delete sessionStorage['id_sku'];
delete sessionStorage['name'];
delete sessionStorage['sku'];
delete sessionStorage['price'];


}
<?php
/*echo getTransactionJs($trans);

foreach ($items as &$item) {
  echo getItemJs($trans['id'], $item);
}*/
?>


</script>

<?php
if ($level == '') {
get_footer();
}
?>