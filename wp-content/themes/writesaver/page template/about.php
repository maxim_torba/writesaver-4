<?php
/*
 * Template Name: about
 */

get_header();
?>
<section>
    <div class="breadcum">
        <div class="container">
            <div class="page_title">
                <?php the_title('<h1>', '</h1>'); ?>
            </div>
        </div>
    </div>
</section>
<section>
    <div class="about_sticky">
        <div class="container">
            <ul>
                <li  class="active scroll"><a href="#about_writesaver">About Writesaver </a></li>
                <li class="scroll"><a href="#proofreader">Why use a proofreader </a></li>                
                <li class="scroll"><a href="#work">How it works</a></li>
                <li class="scroll"><a href="#faq">FAQ</a></li>
            </ul>
        </div>
    </div>
    <?php
    if (have_posts()) :
        while (have_posts()) : the_post();
            the_content();
        endwhile;
    endif;
    ?>
</section>
<?php get_footer(); ?>

<script>

    $(document).on("click", ".proof_dashboard_accor .wpb_wrapper .vc_toggle_simple", function () {
        var id = $(this).attr("id");
        $(".proof_dashboard_accor .wpb_wrapper .vc_toggle_simple:not(#" + id + ")").removeClass('vc_toggle_active');
        $(".proof_dashboard_accor .wpb_wrapper .vc_toggle_simple:not(#" + id + ")").find(".vc_toggle_content").hide();
    });

</script>