<?php
/*
  Theme Name: writesaver
  Theme URI: 192.168.0.87/wp_content/themes/writesaver
  Description: A brief description.
  Version: 1.0
  Author: Admin
  Author URI: http://192.168.0.87/writesaver
 */
?>  
<?php
if (is_front_page()): echo ' <section class="home_footer section" id="section5">';
endif;

global $wpdb;

$user_id = get_current_user_id();
?>
    <footer class="footer">
        <div class="footer__inner">

            <div class="footer__logo-wrap">
                <a href="/" class="footer__logo-link">
                    <img src="<?=get_template_directory_uri() ?>/img/footer/logo_footer.png"  alt="logo">
					<?/*<img src="<?php echo of_get_option('footer_logo'); ?>" class="img-responsive" alt="logo">*/?>
                </a>

                <div class="footer__line"></div>
            </div>

            <div class="footer__navigation">
                <ul class="footer__nav-list">
                    <li class="footer__nav-item">
                        <ul>
						  <?php
                                $current_user = wp_get_current_user();
                                $user_roles_array = $current_user->roles;
                                $user_role = array_shift($user_roles_array);
                                $menu_name = 'Quick Links Menu';
                                $menu = wp_get_nav_menu_object($menu_name);

                                $menuitems = wp_get_nav_menu_items($menu->term_id, array('order' => 'DESC'));
                                _wp_menu_item_classes_by_context($menuitems);
                                foreach ($menuitems as $item):

                                    $classes = $item->classes;
                                    if (in_array('current-menu-item', $classes) || in_array('current-menu-parent', $classes)) {
                                        $classes[] = 'active ';
                                    }
                                    apply_filters('nav_menu_css_class', $classes, $menu_item, $args);
                                    $classes = implode(" ", $classes);

                                    $id = get_post_meta($item->ID, '_menu_item_object_id', true);
                                    $page = get_page($id);
                                    $link = get_page_link($id);
                                    if (($id == 719 && is_user_logged_in()) || ($id == 14 && $user_role != 'customer' && $user_role != '')) {
                                        
                                    } else {
                                        ?>
                                       
							<li>
                                <a class="footer__nav-link" href="<?php echo $link; ?>"> <?php echo $page->post_title; ?></a>
                            </li>
                                        <?php
                                    }
									
                                endforeach;
                                ?>
						
                        </ul>
                    </li>
					
					<?
					    $menu_name = 'customer-service-menu';
                                $menu = wp_get_nav_menu_object($menu_name);

                                $menuitems = wp_get_nav_menu_items($menu->term_id, array('order' => 'DESC'));
                                _wp_menu_item_classes_by_context($menuitems);
					      foreach ($menuitems as $item):

                                    $classes = $item->classes;
                                    if (in_array('current-menu-item', $classes) || in_array('current-menu-parent', $classes)) {
                                        $classes[] = 'active ';
                                    }
                                    apply_filters('nav_menu_css_class', $classes, $menu_item, $args);
                                    $classes = implode(" ", $classes);

                                    $id = get_post_meta($item->ID, '_menu_item_object_id', true);
                                    $page = get_page($id);
                                    $link = get_page_link($id);
				
				
				     endforeach;
					
					
					
					?>
					
					
                    <li class="footer__nav-item">
                     
							<?php
			wp_nav_menu(array(
				'theme_location' => 'customer-service-menu',
				'container'      => 'ul',
				'container_class'=>'',
				'menu_class'     => '',
				'depth' =>1,
				'walker'=> new Mytrue_Walker_Nav_Menu()
			));
					?>
                    </li>
                    <li class="footer__nav-item">
                     
                    </li>
                </ul>
            </div>

            <div class="social">
                <div class="social__inner">
                    <ul class="social__list">
                         <li class="social__item">
                            <a href="<?php echo of_get_option('twitter_link'); ?>" target="_blank" class="social__link twitter">twitter</a>
                        </li>
                        <!--/END social__item-->
                        <li class="social__item">
                            <a href="<?php echo of_get_option('facebook_link'); ?>" target="_blank"
                               class="social__link facebook">facebook</a>
                        </li>
                        <!--/END social__item-->
                        <li class="social__item">
                            <a href="<?php echo of_get_option('instagram_link'); ?>" target="_blank" class="social__link instagram">instagram</a>
                        </li>
                      <li class="social__item">
                            <a href="<?php echo of_get_option('pinterest_link'); ?>" target="_blank" class="social__link reddit">reddit</a>
                        </li>
 

                        <!--/END social__item-->
                    </ul>
                    <!--/END social__list-->
                </div>
                <!--/END social__inner-->
            </div>
            <!--/END social-->
        </div>
    </footer>

<?php
if (is_front_page()): echo '</section></div>';
endif;
?>

<?php
$page_id = get_the_ID();
$display = of_get_option('display_popup');
$display_page = of_get_option('popup_select_pages');
if ($display == 1 && $page_id == $display_page && !isset($_COOKIE['writesaver_cookie_popup'])):
    ?>
    <div id="subscrib_pop" class="pop_overlay open subscrib" style="display: none;">
        <div class="pop_main">
            <div class="pop_head">
                <a href="javascript:void(0);"><i class="fa fa-remove" aria-hidden="true"></i></a>
            </div>
            <div class="pop_body">
                <div class="page_title">
                    <h2>Subscribe Today</h2>
                </div>
                <div class="pop_content">
                    <div class="first_test">
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry</p> 
                        <form method="post" action="<?php echo site_url(); ?>/?na=s" onsubmit="return newsletter_check(this)">
                            <!-- email -->
                            <input class="tnp-email contact_block" type="email" name="ne" size="30" required placeholder="Enter your email"/>
                            <input class="tnp-submit btn_sky" type="submit" value="Subscribe Now"/>                            
                        </form> 
                        <img src="<?php echo get_template_directory_uri(); ?>/images/subscrib_pop-img.png" alt="image" />
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php endif; ?>


<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="<?php echo get_template_directory_uri() ?>/js/hc-sticky.js" type="text/javascript"></script>
<script src="<?php echo get_template_directory_uri() ?>/js/responsive-tabs.js" type="text/javascript"></script>  
<script src="https://cdnjs.cloudflare.com/ajax/libs/smoothscroll/1.4.6/SmoothScroll.js" type="text/javascript"></script>
<!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js" type="text/javascript"></script>-->

<script src="<?php echo get_template_directory_uri() ?>/js/bootstrap.min.js" type="text/javascript"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/fileupload.js"></script>
<script src="<?php echo get_template_directory_uri() ?>/js/jquery.flexslider.js" type="text/javascript"></script>
<script src="<?php echo get_template_directory_uri() ?>/js/jquery.fullPage.min.js" type="text/javascript"></script>
<script src="<?php echo get_template_directory_uri() ?>/js/typed.js" type="text/javascript"></script>

<script src="<?php echo get_template_directory_uri() ?>/js/jquery.validate.js"></script>   
<script src="https://cdn.jsdelivr.net/jquery.validation/1.13.1/additional-methods.js" ></script> 
<!--<script src="//cdn.tinymce.com/4/tinymce.min.js"></script>-->
<script src="<?php echo get_template_directory_uri() ?>/js/jquery.countdown.js" type="text/javascript"></script>
<script src="<?php echo get_template_directory_uri() ?>/js/custom.js<?=ver?>" type="text/javascript"></script>
<script src='<?php echo get_template_directory_uri() ?>/js/timer.jquery.js'></script>
<script src='<?php echo get_template_directory_uri() ?>/js/bootstrap-datepicker.js'></script>

<script src="<?php echo get_template_directory_uri() ?>/js/scriptsall.js<?=ver?>"></script>
<script>
 $('.bill_popup .stripe_pop.pop_btn').click(function (e) {
                              //  debugger;
                                e.stopPropagation();
                                $('#Paypal').fadeIn();
                                $('#Paypal').addClass('open');
                                return false;
                            });
                            $('#Paypal .pop_head a .fa-remove').click(function (e) {
                                e.stopPropagation();
                                $('#Paypal').fadeOut();
                                $('#Paypal').removeClass('open');
                            });
                        $(window).load(function () {
                            var page_id = <?php echo get_the_ID() ?>;
                            if (page_id != 762 && page_id != 545)
                                window.localStorage.removeItem("proof_doc");
                        });
                        jQuery(document).ready(function () {

                            $('.notify_content').on('click', 'ul.notify_list li a', function (e) {
                                e.stopPropagation();
                                var notify_id = $(this).attr("data-notify_id");
                                $.ajax({
                                    type: "POST",
                                    url: "<?php echo admin_url('admin-ajax.php'); ?>",
                                    data: {
                                        action: "read_notification",
                                        'notify_id': notify_id
                                    },
                                    success: function (data) {
                                        $('ul.notify_list').html(data);
                                    },
                                    error: function (jqXHR, textStatus, errorThrown) {
                                      //  console.log(jqXHR + " :: " + textStatus + " :: " + errorThrown);
                                    }
                                });
                                return true;
                            });


                            /*newsletter script START*/
                            if (typeof newsletter_check !== "function") {
                                window.newsletter_check = function (f) {
                                    var re = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-]{1,})+\.)+([a-zA-Z0-9]{2,})+$/;
                                    if (!re.test(f.elements["ne"].value)) {
                                        alert("The email is not correct");
                                        return false;
                                    }
                                    for (var i = 1; i < 20; i++) {
                                        if (f.elements["np" + i] && f.elements["np" + i].required && f.elements["np" + i].value == "") {
                                            alert("");
                                            return false;
                                        }
                                    }
                                    if (f.elements["ny"] && !f.elements["ny"].checked) {
                                        alert("You must accept the privacy statement");
                                        return false;
                                    }
                                    return true;
                                }
                            }
                            /*newsletter script END*/ 

                            /*subscribe now popup script START*/
<?php if ($display == 1 && $page_id == $display_page && !isset($_COOKIE['writesaver_cookie_popup'])) { ?>
                                setTimeout(function () {
                                    $('#subscrib_pop').fadeIn('slow');
                                    $('body.home').addClass('pop_open');
                                }, 4000);
                                $('.pop_head a').click(function () {
                                    $('#subscrib_pop').fadeOut('slow');
                                    $('body').removeClass('pop_open');
                                });
<?php } ?>
                            /*subscribe now popup script START*/

//---------------Change Password validation---------//
                            $("form#profile_pass").validate({
                                errorElement: 'span', //default input error message container
                                errorClass: 'text-danger', // default input error message class
                                rules: {
                                    pass: {
                                        required: true
                                    },
                                    new_pass: {
                                        required: true,
                                        minlength: 6,
                                        pwcheck: true,
                                    },
                                    conf_pass: {
                                        required: true,
                                        minlength: 6,
                                        pwcheck: true,
                                        equalTo: "#new_pass"
                                    }
                                },
                                messages: {
                                    pass: {
                                        required: "Password is required.",
                                    },
                                    "new_pass": {
                                        required: "New password is required.",
                                        minlength: jQuery.validator.format("Please enter at least 6 characters."),
                                        pwcheck: "Password must contain  uppercase,lowercase and digit and special character"

                                    },
                                    "conf_pass": {
                                        required: "Confirm new password is required.",
                                        minlength: "Please enter at least 6 characters.",
                                        pwcheck: "Password must contain  uppercase,lowercase and digit and special character",
                                        equalTo: "Confirm password does not match"
                                    }
                                },
                                errors: function () {
                                    var errorClass = this.settings.errorClass.split(" ").join(".");
                                    return $(this.settings.errorElement + "." + errorClass, this.errorContext);
                                },
                                submitHandler: function (form) {

                                    $('.pass_msg1').remove();
                                    var pass = $('#pass').val();
                                    var new_pass = $('#new_pass').val();
                                    var conf_pass = $('#conf_pass').val();
                                    if (new_pass != conf_pass) {
                                        $('.pass_msg1').remove();
                                        $('.pass_msg').append('<span class="text-danger pass_msg1">Confirmed Password does not match.</span>');
                                        $(".pass_msg1").fadeOut(5000);
                                        return false;
                                    } else if (pass == new_pass) {
                                        $('.pass_msg1').remove();
                                        $('.pass_msg').append('<span class="text-danger pass_msg1">Old password and new password are the same. Please enter a different password.</span>');
                                        $(".pass_msg1").fadeOut(5000);
                                        return false;
                                    } else {
                                        $.ajax({
                                            url: "<?php echo admin_url('admin-ajax.php'); ?>",
                                            type: "POST",
                                            data: {
                                                action: 'save_pass',
                                                pass: pass,
                                                new_pass: new_pass,
                                                conf_pass: conf_pass
                                            },
                                            dataType: "html",
                                            success: function (data) {
                                                if (data == 1) {
                                                    $(".pass_msg").html('<span class="text-success pass_msg1">Password updated successfully......</span>');
                                                    window.setTimeout(function () {
                                                        $('#pass_modal').modal('hide');
                                                    }, 1000);
                                                } else {
                                                    $(".pass_msg").html('<span class="text-danger pass_msg1"> Current Password does not match</span>');
                                                }
                                                $('#pass').val('');
                                                $('#new_pass').val('');
                                                $('#conf_pass').val('');
                                                $(".pass_msg1").fadeOut(5000);
                                            },
                                            error: function (jqXHR, textStatus, errorThrown) {
                                                //console.log(jqXHR + " :: " + textStatus + " :: " + errorThrown);
                                            }
                                        });
                                    }
                                }
                            });
                            $.validator.addMethod("pwcheck", function (value) {
                                return /^[A-Za-z0-9\d!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]*$/.test(value)
                                        && /[A-Z]/.test(value)
                                        && /[a-z]/.test(value)
                                        && /\d/.test(value)
                                        && /[!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]/.test(value)

                            });
                        });

                        // Notification dropdown js

                        $('.notrify > a').click(function (e) {

                            e.preventDefault();

                            e.stopPropagation();

                            if ($(this).hasClass('open')) {

                                $(this).removeClass('open');

                                $('.notrify .notify_content').slideUp('slow');

                            } else if ($('.user > a').hasClass('open')) {

                                $('.user > a').removeClass('open');

                                $('.user .notify_content').slideUp('slow');

                                $(this).addClass('open');

                                $('.notrify .notify_content').slideDown('slow');

                            } else {

                                $(this).addClass('open');

                                $('.notrify .notify_content').slideDown('slow');

                            }

                        });

//                            $('.notrify .notify_content').click(function (e) {
//
//                                e.stopPropagation();
//
//                            });
//
//                            $('.notrify .notify_content a').click(function (e) {
//
//                                e.stopPropagation();
//
//                            });

                        $(document).click(function (e) {

                            e.stopPropagation();

                            $('.notrify a').removeClass('open');

                            $('.notrify .notify_content').slideUp('slow');

                        });



                        // user menu dropdown js

                        $('.user > a').click(function (e) {

                            e.preventDefault();

                            e.stopPropagation();

                            if ($(this).hasClass('open')) {

                                $(this).removeClass('open');

                                $('.user .notify_content').slideUp('slow');

                            } else if ($('.notrify > a').hasClass('open')) {

                                $('.notrify > a').removeClass('open');

                                $('.notrify .notify_content').slideUp('slow');

                                $(this).addClass('open');

                                $('.user .notify_content').slideDown('slow');

                            } else {

                                $(this).addClass('open');

                                $('.user .notify_content').slideDown('slow');

                            }

                        });

                        $('.user .notify_content').click(function (e) {

                            e.stopPropagation();

                        });

                        $('.user .notify_content a').click(function (e) {

                            e.stopPropagation();

                        });

                        $(document).click(function (e) {

                            e.stopPropagation();

                            $('.user a').removeClass('open');

                            $('.user .notify_content').slideUp('slow');

                        });

                        $("#getting-started").countdown("2017/03/17", function (event) {
                            $(this).text(
                                    event.strftime('%H:%M:%S')
                                    );
                        });

                        //Width of open close submitted documents
                        $(document).ready(function () {
                            $('.openre, #OpenAllSubmittedDocuments').bind('click', function () {
                                var collapsible_slider_width = $('.collapsible_slider').width();
                                var create_new_doc_width = $('.create_new_doc').width();
                                var tot_width = collapsible_slider_width - (create_new_doc_width + 1);
                                $('.dashboard_content_slider').outerWidth(tot_width);
                            });
//                    Slider    

                            $(window).on('resize load', function () {
                                setTimeout(function () {
                                    var collapsible_slider_width = $('.collapsible_slider').width();
                                    var create_new_doc_width = $('.create_new_doc').width();
                                    var tot_width = collapsible_slider_width - (create_new_doc_width + 1);
                                    $('.dashboard_content_slider').outerWidth(tot_width);
                                }), 1000;
//                        slider
                                $('.openre, #OpenAllSubmittedDocuments').bind('click', function () {
                                    setTimeout(function () {
                                        if ($(window).width() > 1590 && $(window).width() < 1921) {
                                            $('.dashboard_collapsible_slider').flexslider({
                                                animation: "slide",
                                                controlNav: false,
                                                animationLoop: false,
                                                slideshow: false,
                                                itemWidth: 164,
                                                itemMargin: 30,
                                                move: 1,
                                                minItems: 8,
                                                maxItems: 8
                                            });
                                            if ($('.dashboard_collapsible_slider .slides li').length <= 8) {
                                                $(".dashboard_collapsible_slider .flex-direction-nav").hide();
                                                $(".dashboard_collapsible_slider .flex-direction-nav").hide();
                                            } else {
                                                $(".dashboard_collapsible_slider .flex-direction-nav").show();
                                                $(".dashboard_collapsible_slider .flex-direction-nav").show();
                                            }
                                        } else if ($(window).width() > 1199 && $(window).width() < 1590) {
                                            $('.dashboard_collapsible_slider').flexslider({
                                                animation: "slide",
                                                controlNav: false,
                                                animationLoop: false,
                                                slideshow: false,
                                                itemWidth: 164,
                                                itemMargin: 30,
                                                move: 1,
                                                minItems: 6,
                                                maxItems: 6
                                            });
                                            if ($('.dashboard_collapsible_slider .slides li').length <= 6) {
                                                $(".dashboard_collapsible_slider .flex-direction-nav").hide();
                                                $(".dashboard_collapsible_slider .flex-direction-nav").hide();
                                            } else {
                                                $(".dashboard_collapsible_slider .flex-direction-nav").show();
                                                $(".dashboard_collapsible_slider .flex-direction-nav").show();
                                            }
                                        } else if ($(window).width() > 991 && $(window).width() < 1199) {
                                            $('.dashboard_collapsible_slider').flexslider({
                                                animation: "slide",
                                                controlNav: false,
                                                animationLoop: false,
                                                slideshow: false,
                                                itemWidth: 164,
                                                itemMargin: 20,
                                                move: 1,
                                                minItems: 5,
                                                maxItems: 5
                                            });
                                            if ($('.dashboard_collapsible_slider .slides li').length <= 5) {
                                                $(".dashboard_collapsible_slider .flex-direction-nav").hide();
                                                $(".dashboard_collapsible_slider .flex-direction-nav").hide();
                                            } else {
                                                $(".dashboard_collapsible_slider .flex-direction-nav").show();
                                                $(".dashboard_collapsible_slider .flex-direction-nav").show();
                                            }
                                        } else if ($(window).width() > 767 && $(window).width() < 991) {
                                            $('.dashboard_collapsible_slider').flexslider({
                                                animation: "slide",
                                                controlNav: false,
                                                animationLoop: false,
                                                slideshow: false,
                                                itemWidth: 164,
                                                itemMargin: 30,
                                                move: 1,
                                                minItems: 4,
                                                maxItems: 4
                                            });
                                            if ($('.dashboard_collapsible_slider .slides li').length <= 4) {
                                                $(".dashboard_collapsible_slider .flex-direction-nav").hide();
                                                $(".dashboard_collapsible_slider .flex-direction-nav").hide();
                                            } else {
                                                $(".dashboard_collapsible_slider .flex-direction-nav").show();
                                                $(".dashboard_collapsible_slider .flex-direction-nav").show();
                                            }
                                        } else if ($(window).width() > 567 && $(window).width() < 768) {
                                            $('.dashboard_collapsible_slider').flexslider({
                                                animation: "slide",
                                                controlNav: false,
                                                animationLoop: false,
                                                slideshow: false,
                                                itemWidth: 164,
                                                itemMargin: 30,
                                                move: 1,
                                                minItems: 3,
                                                maxItems: 3
                                            });
                                            if ($('.dashboard_collapsible_slider .slides li').length <= 3) {
                                                $(".dashboard_collapsible_slider .flex-direction-nav").hide();
                                                $(".dashboard_collapsible_slider .flex-direction-nav").hide();
                                            } else {
                                                $(".dashboard_collapsible_slider .flex-direction-nav").show();
                                                $(".dashboard_collapsible_slider .flex-direction-nav").show();
                                            }
                                        } else if ($(window).width() > 479 && $(window).width() < 567) {
                                            $('.dashboard_collapsible_slider').flexslider({
                                                animation: "slide",
                                                controlNav: false,
                                                animationLoop: false,
                                                slideshow: false,
                                                itemWidth: 164,
                                                itemMargin: 30,
                                                move: 1,
                                                minItems: 2,
                                                maxItems: 2
                                            });
                                            if ($('.dashboard_collapsible_slider .slides li').length <= 2) {
                                                $(".dashboard_collapsible_slider .flex-direction-nav").hide();
                                                $(".dashboard_collapsible_slider .flex-direction-nav").hide();
                                            } else {
                                                $(".dashboard_collapsible_slider .flex-direction-nav").show();
                                                $(".dashboard_collapsible_slider .flex-direction-nav").show();
                                            }
                                        } else if ($(window).width() > 300 && $(window).width() < 479) {
                                            $('.dashboard_collapsible_slider').flexslider({
                                                animation: "slide",
                                                controlNav: false,
                                                animationLoop: false,
                                                slideshow: false,
                                                itemWidth: 164,
                                                itemMargin: 30,
                                                move: 1,
                                                minItems: 1,
                                                maxItems: 1
                                            });
                                            if ($('.dashboard_collapsible_slider .slides li').length <= 1) {
                                                $(".dashboard_collapsible_slider .flex-direction-nav").hide();
                                                $(".dashboard_collapsible_slider .flex-direction-nav").hide();
                                            } else {
                                                $(".dashboard_collapsible_slider .flex-direction-nav").show();
                                                $(".dashboard_collapsible_slider .flex-direction-nav").show();
                                            }
                                        } else {
                                            $('.dashboard_collapsible_slider').flexslider({
                                                animation: "slide",
                                                controlNav: false,
                                                animationLoop: false,
                                                slideshow: false,
                                                itemWidth: 164,
                                                itemMargin: 30,
                                                move: 1,
                                                minItems: 8,
                                                maxItems: 8
                                            });
                                            if ($('.dashboard_collapsible_slider .slides li').length <= 8) {
                                                $(".dashboard_collapsible_slider .flex-direction-nav").hide();
                                                $(".dashboard_collapsible_slider .flex-direction-nav").hide();
                                            } else {
                                                $(".dashboard_collapsible_slider .flex-direction-nav").show();
                                                $(".dashboard_collapsible_slider .flex-direction-nav").show();
                                            }
                                        }
                                    }, 1000);
                                });
                            });

//                             $(document).click(function (e){
//                e.stopPropagation();
//                $('.user a').removeClass('open');
//                $('.user .notify_content').slideUp('slow');  
//                $('#Paypal').fadeOut();
//            });
//            //Popup
//            $('.pop_main').click(function (e){
//                e.stopPropagation();
//            });
                           

                        });

                        /* NEWSLETTER POPUP COOKIE - START */

                        $(document).on("click", "#subscrib_pop .pop_head a", function (e) {
                            $("body").removeClass("pop_open");
//                            e.stopPropagation();
                            setCookie("writesaver_cookie_popup", "WritesaverCookie", 1);
                        });

                        function setCookie(cname, cvalue, exdays) {
                            var d = new Date();
                            //d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
                            var minutes = <?php echo of_get_option('popup_select_time'); ?>;
                            d.setTime(d.getTime() + (minutes * 60 * 1000));
                            var expires = "expires=" + d.toUTCString();
                            document.cookie = cname + "=" + cvalue + "; " + expires;
                        }

                        function getCookie(cname) {
                            var name = cname + "=";
                            var ca = document.cookie.split(';');
                            for (var i = 0; i < ca.length; i++) {
                                var c = ca[i];
                                while (c.charAt(0) == ' ') {
                                    c = c.substring(1);
                                }
                                if (c.indexOf(name) == 0) {
                                    return c.substring(name.length, c.length);
                                }
                            }
                            return "";
                        }
                        /* NEWSLETTER POPUP COOKIE - END */
                     //   $(window).trigger(resize);
                        //function resize() {
                           // var editor_width = $(".hidden_scroll").width() + 16.5;
                            //    var editor_tot_width = $(".hidden_scroll").width() + 16.5;
                           // $('.hidden_scroll').css('width', editor_width);
                           // $('.parentscrollcontents').css('width', editor_width);
                      //  }
                        
                        
                        
                        
                        
                        
</script>
<?php 

$current_user->membership_level = pmpro_getMembershipLevelForUser($current_user->ID);
$createddate=$wpdb->get_row("SELECT user_registered FROM wp_users WHERE ID=$user_id ")->user_registered;
$createddateint=strtotime($createddate);

$words_remaining_cust = $wpdb->get_row(" SELECT remaining_credit_words FROM tbl_customer_general_info WHERE fk_customer_id = $user_id ")->remaining_credit_words;
$docs_submitted = $wpdb->get_row(" SELECT total_submited_docs FROM tbl_customer_general_info WHERE fk_customer_id = $user_id ")->total_submited_docs;


$intercomemail=$wpdb->get_row("SELECT user_email FROM wp_users WHERE ID=$user_id")->user_email;
$intercomfirstname=$wpdb->get_row("SELECT meta_value FROM wp_usermeta WHERE user_id=$user_id AND meta_key='first_name'")->meta_value;
$intercomlastname=$wpdb->get_row("SELECT meta_value FROM wp_usermeta WHERE user_id=$user_id AND meta_key='last_name'")->meta_value;
$intercomuserrole=$wpdb->get_row("SELECT meta_value FROM wp_usermeta WHERE user_id=$user_id AND meta_key='role'")->meta_value;
$intercomcreatedat=$createddateint;
$intercomsubscriptionlevel=$current_user->membership_level->name;
$intercomwordsremaining=$words_remaining_cust;

if ($intercomwordsremaining == '') {
$intercomwordsremaining = 0;

}


$intercomdocssubmitted=$docs_submitted;

if ($intercomdocssubmitted=='') {

$intercomdocssubmitted = 0;

}

if ($intercomsubscriptionlevel=='') {

$intercomsubscriptionlevel="None"; }


?>
<script>
var userr = "<?php echo $user_id; ?>";


if (userr > 0) {
  window.intercomSettings = {
    app_id: "f5fyv8vq",
    email: "<?php echo $intercomemail; ?>",
	firstname: "<?php echo $intercomfirstname; ?>",
	lastname: "<?php echo $intercomlastname; ?>",
	created_at: "<?php echo $intercomcreatedat; ?>",
	User_role: "<?php echo $intercomuserrole; ?>",
	Subscription_level: "<?php echo $intercomsubscriptionlevel; ?>",
	Words_remaining: <?php echo $intercomwordsremaining; ?>,
	docs_submitted: <?php echo $intercomdocssubmitted; ?>, 
  }
  
  } else {
  
   window.intercomSettings = {
    app_id: "f5fyv8vq",
  
  }
  }
  
</script>
<script>(function(){var w=window;var ic=w.Intercom;if(typeof ic==="function"){ic('reattach_activator');ic('update',intercomSettings);}else{var d=document;var i=function(){i.c(arguments)};i.q=[];i.c=function(args){i.q.push(args)};w.Intercom=i;function l(){var s=d.createElement('script');s.type='text/javascript';s.async=true;s.src='https://widget.intercom.io/widget/f5fyv8vq';var x=d.getElementsByTagName('script')[0];x.parentNode.insertBefore(s,x);}if(w.attachEvent){w.attachEvent('onload',l);}else{w.addEventListener('load',l,false);}}})()</script>




<?php wp_footer(); ?>
<?/*
<script src="<?php echo get_template_directory_uri() ?>/js/slick/slick.min.js"></script>
<script src="<?php echo get_template_directory_uri() ?>/js/common.js"></script>*/?>
</div>

</body>    

</html>
