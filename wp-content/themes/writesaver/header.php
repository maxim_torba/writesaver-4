<?php
/*
  Theme Name: writesaver
  Theme URI: 192.168.0.87/wp_content/themes/writesaver
  Description: A brief description.
  Version: 1.0
  Author: Admin
  Author URI: http://192.168.0.87/writesaver
 */
 define('ver', '?v=1.3')
?>
<?//if (is_front_page()){	echo '<!DOCTYPE html>';	}?>
<!DOCTYPE html>

    <html <?php language_attributes(); ?> class="no-js"> 

        <head>
            <title>
                <?php if (is_front_page() || is_home()) { ?>
                    <?php bloginfo('name'); ?> | <?php bloginfo('description'); ?>
                <?php } else { ?>
                    <?php wp_title(''); ?> - <?php bloginfo('name'); ?> | <?php bloginfo('description'); ?>
                <?php } ?>
            </title>
            <meta charset="UTF-8">
            <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
            <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
            <link href="<?php echo get_template_directory_uri() ?>/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
            <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/fileupload.css" />
            <link href="<?php echo get_template_directory_uri() ?>/css/responsive-tab.css" rel="stylesheet" type="text/css"/>
            <link href="<?php echo get_template_directory_uri() ?>/css/main.css<?=ver?>" rel="stylesheet" type="text/css"/>
            <link href="<?php echo get_template_directory_uri() ?>/css/flexslider.css" rel="stylesheet" type="text/css"/>
            <link href="<?php echo get_template_directory_uri() ?>/css/jquery.fullpage.min.css" rel="stylesheet" type="text/css"/>
            <link href="<?php echo get_template_directory_uri() ?>/css/font-awesome.css" rel="stylesheet" type="text/css"/>
            <link href="<?php echo get_template_directory_uri() ?>/css/style_uv.css" rel="stylesheet" type="text/css"/>
            <link href="<?php echo get_template_directory_uri() ?>/css/style.css<?=ver?>" rel="stylesheet" type="text/css"/>
            <link href="<?php echo get_template_directory_uri() ?>/css/responsive.css" rel="stylesheet" type="text/css"/>
            <link rel="icon" type="image/x-icon" href="<?php echo of_get_option('favicon_logo'); ?>">
			<link href="<?php echo get_template_directory_uri() ?>/css/main.min.css<?=ver?>" rel="stylesheet" type="text/css"/>
			<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>  

            <?php wp_head(); ?>
			

        </head>   

        <!--        <div id="wptime-plugin-preloader"></div>-->

        <body <?php body_class(); ?>>
		<div class="all_wrapper">
            <?php date_default_timezone_set(get_option('timezone_string')); ?>
            <div class="load_overlay" id="loding">
                <img src="<?php echo get_template_directory_uri(); ?>/images/39.gif"/>
            </div>
            <!--Top Section Start-->
            <?php if (get_the_ID() != 1233 && get_the_ID() != 1211) { ?>
                <header class="top-head <?php echo (is_user_logged_in() ? "with_login" : "") ?>">
                    
						<div class="top-head__inner">

						<div class="top-logo">
							<a href="<?php echo home_url(); ?>" class="top-logo__link">
								<img src="<?php echo of_get_option('header_logo') ?>" alt="logo">
							</a>
							
						</div>
						<!--/END logo-wrap-->

						<div class="burger-btn-wrap">
							<button class="burger-btn"></button>
						</div>

						<div class="nav-wrap">

							<nav class="navigation">
								<ul class="navigation__list">
                                        <?php
                                        $current_user = wp_get_current_user();
                                        $user_roles_array = $current_user->roles;
                                        $user_role = array_shift($user_roles_array);
										
										//echo '<pre>'; print_r( $user_role); echo '</pre>';
										
										
                                        $menu_name = 'Header Menu';
                                        $menu = wp_get_nav_menu_object($menu_name);

                                        $menuitems = wp_get_nav_menu_items($menu->term_id, array('order' => 'DESC'));
                                        _wp_menu_item_classes_by_context($menuitems);
                                        foreach ($menuitems as $item):

                                            $classes = $item->classes;
                                            if (in_array('current-menu-item', $classes) || in_array('current-menu-parent', $classes)) {
                                                $classes[] = 'active ';
                                            }
                                            apply_filters('nav_menu_css_class', $classes, $menu_item, $args);
                                            $classes = implode(" ", $classes);

                                            $id = get_post_meta($item->ID, '_menu_item_object_id', true);
											
																				
											
                                            $page = get_page($id);
                                            $link = get_page_link($id);
                                            if (($id == 719 && is_user_logged_in()) ||  ($id == 6 && $user_role  == 'customer' && $user_role != '')) {
												///***Було $id == 14
                                                
                                            } else {
                                                ?>
                                                <li class="navigation__item">
                                                    <a href="<?php echo $link; ?>" class="navigation__link">
                                                        <?php echo $page->post_title; ?>
                                                    </a>
                                                </li>
                                                <?php
                                            }
									
											
                                        endforeach;
                                        ?>
									<!--/END navigation__item-->
								</ul>
								<!--/END navigation__list-->
								<div class="navigation__bot-line">
									<div class="navigation__line"></div>
								</div>
							</nav>
							<!--/END navigation-->

							
							
							
							
							
							
							
							
							
							
							
							
							
							
					<?php if (!is_user_logged_in()) { ?>
						
				<div class="top-head__login-wrap">
					<div class="top-head__login">
							<a class="top-head__login-link" href="javascript:void(0);" onclick="open_loginpop('login');">Log In</a>
						<a class="top-head__login-link" href="javascript:void(0);" onclick="open_loginpop('register');">Register</a>
                    </div>
                </div>
						
						
										
                        <?php	echo '</div>';?>
						
						<div id="login_regi_modal" class="pop_overlay" style="display: none; ">
                            <div class="pop_main" style="min-height: 600px;">
                                <div class="pop_head">
                                    <a href="javascript:void(0);"><i class="fa fa-remove" aria-hidden="true"></i></a>
                                </div>
                                <div class="pop_body">
                                    <div class="page_title">
                                        <ul class="nav nav-tabs">
                                            <li class="active"><a data-toggle="tab" href="#login_pop">Login</a></li>
                                            <li><a data-toggle="tab" href="#regi_pop">Register</a></li>
                                        </ul>
                                    </div>
                                    <div class="pop_content">
                                        <div class="tab-content">
                                            <div id="login_pop" class="tab-pane fade in active">

                                                <form name="signin" id="signin" method="post" action="#">
                                                    <?php if ($_REQUEST['plan']): ?>		
                                                        <input type="hidden" class="hid_mamu_level_id" value="<?php echo $_REQUEST['plan']; ?>" />		
                                                        <input type="hidden" value="<?php echo get_the_permalink(14); ?>" class="plan_url" />		
                                                    <?php endif; ?>
                                                   <h3 style='padding: 20px;'>Sign In</h3>

                                                    <input type="email" placeholder="Email Address*" class="contact_block" name="email" required="">

                                                    <input type="password" placeholder="Password*" class="contact_block" name="pw" required="">
                                                    <div class="remember">
                                                        <input name="remember" id="remember" type="checkbox">
                                                        <label for="remember">Remember me</label>
                                                    </div>
                                                    <a class="forgot_pass" href="<?php echo get_the_permalink(747); ?>">Forgot Password?</a>

                                                    <div class="sub_btn">                                        

                                                        <input type="submit" class="btn_sky" value="Login">    

                                                    </div>                                    
                                                    <div class="msg" id="signin_msg" ></div>
                                                </form>

                                                <div class="other_login">
                                                    <span>or</span>
                                                </div>

                                                <div class="social_login">
                                                    <?php echo do_shortcode('[TheChamp-Login]'); ?>
                                                </div>
                                            </div>
                                            <div id="regi_pop" class="tab-pane fade">
                                                <section class="login" id="register_section">
                                                    <form name="signup"  id="signup" method="post" action="">
                                                        <h3 style='padding: 20px;'>Create your Account</h3> 
                                                        <input type="hidden" name="hid_level_id"  id="hid_level_id" value="" />
                                                        <input type="hidden" name="role" class="role" value="customer">
                                                        <div class="row">
                                                            <div class="col-sm-6">
                                                                <input type="text" placeholder="First Name*" class="only_alpha contact_block" maxlength="50" name="fname" required="">
                                                            </div>
                                                            <div class="col-sm-6">
                                                                <input type="text" placeholder="Last Name*" class="only_alpha contact_block" maxlength="50" name="lname" required="">
                                                            </div>
                                                        </div>
                                                        <input type="text" placeholder="Email*" class="contact_block" name="email" maxlength="50">
                                                        <input type="password" maxlength="20" placeholder="Password*" class="contact_block" name="pw" id="pw" required="">
                                                        <input type="password" maxlength="20" placeholder="Confirm Password*" class="contact_block" name="cpw"  required="">
                                                        <div class="remember">
                                                            <input id="reg_remember" type="checkbox" name="remember" /><label for="reg_remember">I agree with the <span><a href="<?php echo get_page_link(35); ?>">terms</a></span> and <span><a href="<?php echo get_page_link(33); ?>">privacy policy</a></span></label>
                                                            <span class="form-error text-danger " id="spn_ChkAgree" style="display:none;" >Please accept the terms of service before continuing</span>
                                                        </div>
                                                        <div class="sub_btn">
                                                            <input type="submit" name="signup" id="signupbtn" class="btn_sky" value="Register">
                                                        </div>
                                                        <div class="msg" id="signup_msg" ></div>
                                                    </form>
                                                    <div class="other_login">
                                                        <span>or</span>
                                                    </div>
                                                    <div class="social_login row">
                                                        <?php echo do_shortcode('[TheChamp-Login]'); ?>
                                                    </div> 
                                                </section>
                                                <div id="thankyou_section" style="display: none;">
                                                    <section class="login">
                                                        <div class="registration_thank_you">
                                                            <div class="row">
                                                                <div class="col-md-offset-3 col-sm-6">
                                                                    <div class="thank-you">
                                                                        <div class="thank_msg">
                                                                            <div class="thank_img">
                                                                                <img src="<?php echo get_template_directory_uri() ?>/images/check_thanku.png" alt="images">
                                                                            </div>
                                                                            <h2>Registration successful! Confirm Your account now to access Writesaver.</h2>
                                                                            <p>We sent a confirmation email to <span class="user_mail"></span>. Check your inbox and press the confirmation button to access your account.</p>
                                                                            <div class="thank_confirm_email">
                                                                                <p>Didn't get your Confirmation Email?</p>
                                                                                <p>Check your spam folder.</p>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>                                                                                                                
                                                    </section>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>  
						
						
						<?
						
                    } else {  echo '</div>';
                        $user_id = get_current_user_id();
                        $key1 = 'first_name';
                        $key2 = 'last_name';
                        $single = true;
                        $user_first = ucwords(get_user_meta($user_id, $key1, $single));
                        $current_user = wp_get_current_user();
                        $user_roles_array = $current_user->roles;
                        $user_role = array_shift($user_roles_array);
                        $page_id = get_the_ID();

                        $profile_pic = get_user_meta($user_id, 'profile_pic_url', true);

                        if ($profile_pic == '')
                            $profile_pic = get_template_directory_uri() . '/images/customer.png';
                        ?>
                        <div class="login_home">
                            <ul class="desktop_login"> 
                                <?php
                                if ($page_id == 762 || $page_id == 810)
                                    $class = 'active';
                                else
                                    $class = '';
                                if ($user_role == 'customer') {
                                    echo ' <li class=' . $class . '><a  href="' . get_the_permalink(762) . '">Dashboard</a></li>';
                                } else if ($user_role == 'proofreader') {
                                    echo ' <li class=' . $class . '><a href="' . get_the_permalink(810) . '">Dashboard</a></li>';
                                }

                                if ($user_role == 'customer') {
                                    $notification_list = $wpdb->get_results("SELECT * FROM `tbl_customer_notifications` WHERE fk_customer_id= $user_id AND is_view = 0 ORDER BY pk_cust_notification_id DESC LIMIT 5");
                                } else if ($user_role == 'proofreader') {
                                    $notification_list = $wpdb->get_results("SELECT * FROM `tbl_proofreader_notifications` WHERE fk_proofreader_id= $user_id AND is_view = 0 ORDER BY pk_proof_notification_id DESC LIMIT 3");
                                }
                                if ($user_role == 'customer' || $user_role == 'proofreader') {
                                    /* echo "<pre>";
                                      print_r($notification_list);
                                      echo "</pre>"; */
                                    ?>                   
                                    <li class="notrify">
                                        <a class="open_noti" href="javascript:void(0);"></a>
                                        <div class="notify_content" style="display: none">
                                            <ul class="notify_list">
                                                <?php
                                                foreach ($notification_list as $notifications) {
                                                    if (count($notification_list) > 0) {
                                                        if ($user_role == 'customer')
                                                            $notify_id = $notifications->pk_cust_notification_id;
                                                        elseif ($user_role == 'proofreader')
                                                            $notify_id = $notifications->pk_proof_notification_id;
                                                        ?>
                                                        <li class="notification">
                                                            <i class="fa fa-bell-o" aria-hidden="true"></i>
                                                            <a data-notify_id="<?php echo $notify_id; ?>" href="javascript:void(0);"><?php echo substr($notifications->description, 0, 15); ?>...</a>
                                                            <span> 
                                                                <?php
                                                                $date = new DateTime($notifications->notification_date);
                                                                echo ago($date->format('U'));
                                                                ?> 
                                                            </span>
                                                        </li>
                                                        <?php
                                                    }
                                                }
                                                if ($user_role == 'customer') {
                                                    $view_notification = get_permalink(1119);
                                                } else if ($user_role == 'proofreader') {
                                                    $view_notification = get_permalink(1121);
                                                }
                                                ?>
                                                <li class="view_all">
                                                    <a href="<?php echo $view_notification; ?>">View all notification</a>                                    
                                                </li>

                                            </ul>
                                        </div>
                                    </li>
                                <?php } ?>
                                <li class="user">
                                    <a href="javascript:void(0);">
                                        <img src="<?php echo $profile_pic; ?>" alt="img" class="img-circle" /> 
                                        <span><strong>Welcome</strong><span class="user_fname"><?php echo $user_first; ?></span></span>

                                        <i class="fa fa-angle-down" aria-hidden="true"></i>
                                    </a>
                                    <div class="notify_content" style="display:none">
                                        <?php
                                        if ($user_role == 'customer') {
                                            $defaults1 = array(
                                                'echo' => true,
                                                'theme_location' => 'customer-dropdown-sub-menu'
                                            );
                                            wp_nav_menu($defaults1);
                                        } else if ($user_role == 'proofreader') {
                                            $defaults1 = array(
                                                'echo' => true,
                                                'theme_location' => 'proofreader-dropdown-sub-menu'
                                            );
                                            wp_nav_menu($defaults1);
                                        } else {
                                            ?>
                                            <ul>
                                                <li>  <a href="<?php echo wp_logout_url(home_url()); ?>">Log out</a></li>
                                            </ul>                                  
                                        <?php } ?> 
                                    </div>
                                </li>
                            </ul>                  
                        </div>
                    <?php } ?>
							
							
							
							
							
							
							
							
							
							
							
					
					</div>
					
					
					
					
					
					
					
					
                </header>
                <?php
            }
            if (is_front_page()): echo '<div id="fullpage">';

            endif;
            ?>
            <input type="hidden" value="<?php echo admin_url('admin-ajax.php'); ?>" id="ajax_url" />