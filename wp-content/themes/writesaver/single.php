<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
add_filter('wp_head', 'rel_link_wp_head_new', 10, 0 );
get_header();


?>
<section>
    <div class="breadcum">
        <div class="container">
            <div class="page_title">
                <h1>Blog</h1>
            </div>
        </div>
    </div>
</section>
<section>
    <div id="main_blog" >
        <div class="blog_category_sticky">
            <div class="container">
                <div class="blog_category_sticky_right">
                    <div class="search_box">
                        <?php get_search_form(); ?>
                    </div>
                </div>
                <div class="blog_category_sticky_left">
                    <div class="desktop_catagory">
                        <div class="category_full_list">
                            <div class="category_btn">
                                <div class="category_btn_icon">
                                    <img src="<?php echo get_template_directory_uri() ?>/images/cat_icon.png" class="img-responsive">
                                </div>
                                <div class="category_btn_txt">
                                    <span>Categories</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> 
            </div>
        </div>
        <div class="blog_category_main blog_details">
            <div class="container">
                <?php $imgIter=1; while (have_posts()) : the_post(); ?>
                    <div id="notContent_1" class="blog_listt"> 
                        <div class="blog_block">
                            <?php the_title('<h1>', '</h1>'); ?>
							<? $title= get_the_title();
							
						
							?>
                            <div class="blog_img">
                                <img src="<?php the_post_thumbnail_url(array(1140, 478)); ?>" class="img-responsive" title="<?titleImg('Blog'); echo $imgIter;?>" alt="<?titleImg('Blog'); echo $imgIter;?>">
                            </div>
                            <div class="blog_detail_desc">
                                <div class="blog_date_category">
                                    <p><?php the_date(); ?></p>
                                    <?php
                                    $category_detail = get_the_category(get_the_ID()); //$post->ID
                                    if ($category_detail):
                                        $cat_count = count($category_detail);
                                        $count = 0;
                                        echo ' <span>';
                                        foreach ($category_detail as $cd) {
                                            $count ++;
                                            echo $cd->cat_name;
                                            if ($count != $cat_count)
                                                echo ', ';
                                        }
                                        echo '</span>';
                                    endif;
                                    ?>                                  
                                </div>
                                <div class="blog_social">
                                    <ul>
                                        <?php echo do_shortcode('[TheChamp-Sharing style="background-color:#fffff;"]') ?>
                                    </ul>
                                </div>
                                <div class="blog_data_desc">
                                    <?php the_content(); ?>
                                </div>
                            </div>      
                        </div>
                    </div>  
                <?php $imgIter++; endwhile; ?>
            </div>
        </div>
    </div>
</section>

<?php get_footer(); ?>
